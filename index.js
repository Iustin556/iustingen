const Discord = require("discord.js");
const bot = new Discord.Client();
const config = require("./config.json");
const prefix = config.prefix;
const botname = "IustinGen";
const prefix1 = ".";
var fs = require("fs");
var lineReader = require("line-reader");
var async = require("async");
const firstline = require("firstline");
const generated = new Set();
var os = require("os");
var express = require('express');
var app = express();
const chalk = require('chalk');

  bot.on('ready', msg => {
  console.log("");                                   
  console.log((chalk.cyan(`                                            #####                                      #####                `)));
  console.log((chalk.cyan(`                                           #     #   ##   #        ##    ####  #    # #     # ###### #    # `)));
  console.log((chalk.cyan(`                                           #        #  #  #       #  #  #    # #   #  #       #      ##   # `)));
  console.log((chalk.cyan(`                                           #  #### #    # #      #    # #      ####   #  #### #####  # #  # `)));
  console.log((chalk.cyan(`                                           #     # ###### #      ###### #      #  #   #     # #      #  # # `)));
  console.log((chalk.cyan(`                                           #     # #    # #      #    # #    # #   #  #     # #      #   ## `)));
  console.log((chalk.cyan(`                                            #####  #    # ###### #    #  ####  #    #  #####  ###### #    # `)));
  console.log("");                                  
  console.log((chalk.yellow(`                                                               Crée par GalackQSM#7926 !`)));  
  console.log((chalk.yellow(`                                                                © 2020 GalackQSM, Inc.`))); 
  console.log("");                                   
  console.log((chalk.red(`                                                         Discord: https://discord.gg/XH7zQ8s`)));   
  console.log((chalk.red(`                                                       Twitter: https://twitter.com/Galack_QSM`)));   
  console.log((chalk.red(`                                                        Github: https://github.com/GalackQSM`)));   
  console.log((chalk.red(`                                                        Youtube: https://youtube.com/GalackQSM`)));   
  console.log("");                                  

  console.log(`Statistiques globales : \n\nLe bot a un total de ${bot.guilds.cache.size} server. \nPour un total de ${bot.users.cache.size} membres.`)
  console.log("Connecté en tant que " + bot.user.id + " | Prefix : " + prefix1 + " | Nombre de sever "+ bot.guilds.cache.size +" | Nombres de salons "+ bot.channels.cache.size +" | Utilisateur totaux "+ bot.users.cache.size +" | Nombre d'emojis totaux "+ bot.emojis.cache.size +'');
  bot.user.setActivity(".help - IustinGen");
});

bot.on("message", message => {
    if (message.channel.id === config.botChannel) { 
        if (message.author.bot) return;
        var command = message.content
            .toLowerCase()
            .slice(prefix.length)
            .split(" ")[0];

        if (command === "gen") {
            if (generated.has(message.author.id)) {
                message.channel.send(
                    "Cooldown 15 minutes! - " +
                    message.author.tag
                );
            } else {
                let messageArray = message.content.split(" ");
                let args = messageArray.slice(1);
                if (!args[0])
                    return message.reply("Veuillez fournir un service!");
                var fs = require("fs");
                const filePath = __dirname + "/comptes/" + args[0] + ".txt";

                const embed = {
                    title: "Stoc epuizat!",
                    description: "Serviciul pe care l-ați solicitat este în prezent Stoc epuizat!",
                    color: 0xff033d,
                    timestamp: new Date(),
                    footer: {
                        icon_url:
                            "https://i.imgur.com/29K1nWf.png",
                        text: "Developer Iustin2019#9851"
                    },
                    image: {url:"https://i.imgur.com/29K1nWf.png"},
                    author: {
                        name: botname + " - generator de cont",
                        url: "https://discord.gg/XH7zQ8s",
                        icon_url: bot.displayAvatarURL
                    },
                    fields: []
                };

                fs.readFile(filePath, function (err, data) {
                    if (!err) {
                        data = data.toString();
                        var position = data.toString().indexOf("\n");
                        var firstLine = data.split("\n")[0];
                        if(position == -1)
                        return message.channel.send({ embed });
                        message.author.send(firstLine);
                        if (position != -1) {
                            data = data.substr(position + 1);
                            fs.writeFile(filePath, data, function (err) {
                                const embed = {
                                    title: "Cont " + args[0] + " generat!",
                                    description: "Contul dumneavoastra a fost trimis in DM!",
                                    color: 0xff033d,
                                    timestamp: new Date(),
                                    footer: {
                                        icon_url: "https://i.imgur.com/29K1nWf.png",
                                        text: "Developer Iustin2019#9851"
                                    },
                                    image: {
                                        url:
                                            "https://i.imgur.com/X1QIYyS.gif"
                                    },
                                    author: {
                                        name: botname + " - generator de cont",
                                        url: "https://discord.gg/XH7zQ8s",
                                        icon_url: bot.displayAvatarURL
                                    },
                                    fields: []
                                };
                                message.channel.send({ embed });
                                generated.add(message.author.id);
                                setTimeout(() => {
                                    generated.delete(message.author.id);
                                }, 150000); // 86400000 = 24 H , 150000 = 15 Min
                                if (err) {
                                    console.log(err);
                                }
                            });
                        } else {
                            message.channel.send("Stoc epuizat!");
                        }
                    } else {
                        const embed = {
                            title: "Serviciu negăsit!",
                            description: "Serviciul solicitat nu poate fi găsit!",
                            color: 0xff033d,
                            timestamp: new Date(),
                            footer: {
                                icon_url:
                                    "https://i.imgur.com/29K1nWf.png",
                                text: "Developer Iustin2019"
                            },
                            image: {url:"https://i.imgur.com/29K1nWf.png"},
                            author: {
                                     name: botname + " - generator de cont",
                                     url: "https://discord.gg/XH7zQ8s",
                                icon_url: bot.displayAvatarURL
                            },
                            fields: []
                        };
                        message.channel.send({ embed });
                        return;
                    }
                });
            }
        }
        else
            if (command === "stats") {
                const embed = {
                    title: "Stats de " + botname,
                    description: "Numărul total de utilizatori: `" + bot.users.cache.size + " membri`\nNumele total al camerei: `" + bot.channels.cache.size+ " saloane`\nNumele total de emoji: `" + bot.emojis.cache.size+ " emoji`\nNumele total al camerei: `" + bot.guilds.cache.size+ " server(s)`",
                    color: 0xff033d,
                    timestamp: new Date(),
                    footer: {
                        icon_url:
                            "https://i.imgur.com/29K1nWf.png",
                        text: "Developer Iustin2019#9851"
                    },
                    image: {url:"https://i.imgur.com/29K1nWf.png"},
                    author: {
                         name: botname + " - generator de cont",
                         url: "https://discord.gg/XH7zQ8s",
                        icon_url: bot.displayAvatarURL
                    },
                    fields: []
                };
                message.channel.send({ embed });
            }
        
            if (command === "help") {

                const embed = {
                    color: 0xff033d,
                    title: botname + ' - generator de cont',
                    url: 'https://i.imgur.com/29K1nWf.png',
                    author: {
                        name: 'lista de comenenzi!',
                        url: 'https://i.imgur.com/29K1nWf.png',
                    },
                    image: {url:"https://i.imgur.com/29K1nWf.png"},

                    description: '**Aceasta este o listă cu toate comenzile**',
                    fields: [
                        {
                            name: 'Generați conturi',
                            value: "Exemple: `" + prefix1 +"gen <Numele serviciului>`",
                        },
                        {
                            name: 'Creați un serviciu',
                            value: "Exemple: `" + prefix1 +"create <Numele serviciului>`",
                        },
                        {
                            name: 'Restock',
                            value: "Exemple: `" + prefix1 +"restock <Numele serviciului> <Numărul de conturi>`",
                        },
                        {
                            name: 'Adăugați conturi',
                            value: "Exemple: `" + prefix1 +"add <mail:pass> <Numele serviciului>`",
                        },
                        {
                            name: 'Vizualizați statisticile despre bot!' + botname,
                            value: "Exemple: `" + prefix1 +"stats`",
                        },
                    ],
                    timestamp: new Date(),
                    footer: {
                        text: 'Developer Iustin2019#9851',
                        icon_url: 'https://i.imgur.com/29K1nWf.png',
                    },
                };
                message.channel.send({ embed });
            }

        if (command === "add") {
            if (!message.member.hasPermission("ADMINISTRATOR"))
                return message.reply("Nu aveți permisiunile pentru a face acest lucru!");
            var fs = require("fs");
            let messageArray = message.content.split(" ");
            let args = messageArray.slice(1);
            var account = args[0]
            var service = args[1]
            if(!account) return message.reply("Furnizați mai întâi un șir de cont formatat!")
            if(!service) return message.reply("Furnizați mai întâi servicii!")
            const filePath = __dirname + "/comptes/" + args[1] + ".txt";
            fs.appendFile(filePath, os.EOL + args[0], function (err) {
                if (err) return console.log(err);
                const embed = {
                    title: "Cont adăugat!",
                    description: "Contul a fost adăugat cu succes la! `" + service + "`!",
                    color: 0xff033d,
                    timestamp: new Date(),
                    footer: {
                        icon_url:
                            "https://i.imgur.com/29K1nWf.png",
                        text: "Developer Iustin2019#9851"
                    },
                    image: {url:"https://i.imgur.com/29K1nWf.png"},
                    author: {
                        name: botname + " - generator de cont",
                        url: "https://discord.gg/XH7zQ8s",
                        icon_url: bot.displayAvatarURL
                    },
                    fields: []
                };
                message.channel.send({ embed });
            });


        }
        if (command === "create") {
            if (!message.member.hasPermission("ADMINISTRATOR"))
                return message.reply("Nu aveți permisiunile pentru a face acest lucru!");
            var fs = require("fs");
            let messageArray = message.content.split(" ");
            let args = messageArray.slice(1);
            const filePath = __dirname + "/comptes/" + args[0] + ".txt";
            fs.writeFile(filePath, 'GalackQSM:GalackQSM', function (err) {
                if (err) throw err;
                const embed = {
                    title: "Serviciu creat!",
                    description: "Serviciul a fost creat cu succes! `" + args[0] + "`!",
                    color: 0xff033d,
                    timestamp: new Date(),
                    footer: {
                        icon_url:
                            "https://i.imgur.com/29K1nWf.png",
                        text: "Developer Iustin2019#9851"
                    },
                    image: {url:"https://i.imgur.com/29K1nWf.png"},
                    author: {
                        name: botname + " - generator de cont",
                        url: "https://discord.gg/XH7zQ8s",
                        icon_url: bot.displayAvatarURL
                    },
                    fields: []
                };
                message.channel.send({ embed });
            });
        }
        if (command === "restock") {
            const embed = {
                title: "Vă mulțumim că ați pus un serviciu!",
                description: "Vă rugăm să furnizați numele serviciului completat!",
                color: 0xff033d,
                timestamp: new Date(),
                footer: {
                    icon_url:
                        "https://i.imgur.com/29K1nWf.png",
                    text: "Developer Iustin2019#9851"
                },
                 image: {url:"https://i.imgur.com/29K1nWf.png"},
                author: {
                    name: botname + " - generator de cont",
                    url: "https://discord.gg/XH7zQ8s",
                    icon_url: bot.displayAvatarURL
                },
                fields: []
            };
            let messageArray = message.content.split(" ");
            let args = messageArray.slice(1);
            if (!message.member.hasPermission("ADMINISTRATOR"))
                return message.reply("Nu aveți permisiunile pentru a face acest lucru!");
            if (!args[0])
            {
                return message.channel.send({ embed });
            }
            if (!args[1])
            {
                return message.channel.send({ embed });
            }
            else {
            message.channel.send("@everyone\n● Restock! **" + args[0] + "**\n● Numărul de conturi adugate: **" + args[1] + " conturi**\n● Restock aduagat de: " + "<@" + message.author.id +">");
            }
        }
    }
});

bot.login(config.token);
